#!/bin/bash


# Note - the mksurfdat utilit was broken during some v3 updates. The errors 
# mention something about "mkFertMod"
# the current workaround is to drop back to an older version of the model, 
# the commit/hash I checked out was: cda33a1902c2e4490bd446e81951e2580071a697

# see Gautum's notes here:
# https://gist.githubusercontent.com/bishtgautam/c93e5d568a15e8fff3c4470ddb2f8519/raw/84b10471c0a42e11ba3ff2dc3dc1b49463ae9068/gistfile1.txt

#-------------------------------------------------------------------------------

e3sm_root=/global/homes/w/whannah/E3SM/E3SM_SRC4
cd ${e3sm_root}/components/elm/tools/mkmapdata


NE=70

HICCUP_ROOT=/global/cfs/projectdirs/m3312/whannah/HICCUP
GRID_FILE_ROOT=${HICCUP_ROOT}/files_grid
MAP_FILE_ROOT=${HICCUP_ROOT}/files_map

INPUTDATA_ROOT=/global/cfs/cdirs/e3sm/inputdata

DST_GRID=${GRID_FILE_ROOT}/scrip_ne${NE}pg2.nc
# MAP_FILE=${MAP_FILE_ROOT}/map_1x1_to_ne${NE}pg2_traave.20240313.nc


${e3sm_root}/components/elm/tools/mkmapdata/mkmapdata.sh --gridfile ${DST_GRID} --inputdata-path ${INPUTDATA_ROOT} --res ne${NE}pg2 --gridtype global --output-filetype 64bit_offset --debug -v --list

#-------------------------------------------------------------------------------
# Download needed input grid files

#!/bin/bash
e3sm_inputdata_repository="https://web.lcrc.anl.gov/public/e3sm"
cesm_inputdata_repository="https://svn-ccsm-inputdata.cgd.ucar.edu/trunk"
inputdata_list=clm.input_data_list
cat $inputdata_list | while read line; do
    localpath=`echo ${line} | sed 's:.* = \(.*\):\1:'`
    url1=${e3sm_inputdata_repository}/`echo ${line} | sed 's:.*\(inputdata/lnd/.*\):\1:'`
    url2=${cesm_inputdata_repository}/`echo ${line} | sed 's:.*\(inputdata/lnd/.*\):\1:'`
    if [ ! -f ${localpath} ]; then
        echo "${url1} -> ${localpath}"
        mkdir -p `dirname ${localpath}`
        cd `dirname ${localpath}`
        # Try to download using first URL, if that fails then use the second
        wget ${url1} || wget ${url2}
    else
        echo "${localpath} exists, skipping."
    fi
done

#-------------------------------------------------------------------------------

# conda create --name ncl_env -c conda-forge ncl netcdf4 nco

conda activate ncl_env

NE=70
HICCUP_ROOT=/global/cfs/projectdirs/m3312/whannah/HICCUP
DATA_FILE_ROOT=${HICCUP_ROOT}/files_fsurdat
GRID_FILE_ROOT=${HICCUP_ROOT}/files_grid

DST_GRID=${GRID_FILE_ROOT}/scrip_ne${NE}pg2.nc

OUTPUT_ROOT=${DATA_FILE_ROOT} ${e3sm_root}/components/elm/tools/mkmapdata/mkmapdata.sh --gridfile ${DST_GRID} --inputdata-path ${INPUTDATA_ROOT} --res ne${NE}pg2 --gridtype global --output-filetype 64bit_offset -v

#-------------------------------------------------------------------------------