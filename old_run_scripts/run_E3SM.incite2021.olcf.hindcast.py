#!/usr/bin/env python
# script for running E3SM-MMF simulations using the 2020 INICTE allocation (CLI115)
# Branch for this campaign: https://github.com/E3SM-Project/E3SM/tree/whannah/incite-2020
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
import os, numpy as np, subprocess as sp, datetime
newcase,config,build,clean,submit,continue_run = False,False,False,False,False,False

acct = 'cli115'
case_dir = os.getenv('HOME')+'/E3SM/Cases/'
src_dir  = os.getenv('HOME')+'/E3SM/E3SM_SRC2/'

### flags to control config/build/submit sections
# clean        = True
# newcase      = True
# config       = True
# build        = True 
submit       = True
# continue_run = True

### run duration
stop_opt,stop_n,resub = 'ndays',1,4

### same settings for all runs
compset = 'F-MMFXX'
arch    = 'GNUGPU'
ne,npg  = 45,2
grid    = f'ne{ne}pg{npg}_r05_oECv3'
rad_nx  = 4
crm_nx  = 32
crm_ny  = 32   # note momentum FB enabled when crm_ny>1

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# simple setup for testing output
# stop_opt,stop_n,resub = 'ndays',1,0
# crm_ny = 4
# walltime = '2:00'
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

iyr,imn,idy = 2008,10,1
init_date = f'{iyr}-{imn:02d}-{idy:02d}'


### common attributes for all cases
# case_list = ['E3SM','INCITE2021-HC','HC',arch,grid,compset,f'NXY_{crm_nx}x{crm_ny}']
case_list = ['E3SM','INCITE2021-HC',arch,grid,compset,f'NXY_{crm_nx}x{crm_ny}']

#---------------------------------------------------------------------------------------------------
### specific case names and settings
#---------------------------------------------------------------------------------------------------
# walltime='4:00' ;case='.'.join(case_list+['DX_1600','L_50_48'  ,'DT_5e0',init_date] )
# walltime='6:00' ;case='.'.join(case_list+['DX_1600','L_125_115','DT_2e0',init_date] )
# walltime='6:00' ;case='.'.join(case_list+['DX_1600','L_250_230','DT_1e0',init_date] ) # NaNs / neg layer thickness
# walltime='10:00';case='.'.join(case_list+['DX_1600','L_250_230','DT_5e-1',init_date] ) 

# walltime='10:00';case='.'.join(case_list+['DX_200','L_50_48'  ,'DT_5e-1',init_date] )
# walltime='6:00' ;case='.'.join(case_list+['DX_200','L_125_115','DT_5e-1',init_date] )
# walltime='10:00' ;case='.'.join(case_list+['DX_200','L_250_230','DT_5e-1',init_date] ) # crashing
# walltime='12:00' ;case='.'.join(case_list+['DX_200','L_250_230','DT_2e-1',init_date] )

### cases with variance transport
# walltime='2:00' ;case='.'.join(case_list+['DX_1600','L_50_48'  ,'DT_5e0' ,'BVT',init_date] )
# walltime='2:00' ;case='.'.join(case_list+['DX_1600','L_125_115','DT_2e0' ,'BVT',init_date] )
# walltime='6:00' ;case='.'.join(case_list+['DX_1600','L_250_230','DT_5e-1','BVT',init_date] ) 

# walltime='6:00' ;case='.'.join(case_list+['DX_200' ,'L_50_48'  ,'DT_5e-1','BVT',init_date] )
# walltime='6:00' ;case='.'.join(case_list+['DX_200' ,'L_125_115','DT_5e-1','BVT',init_date] )
walltime='6:00' ;case='.'.join(case_list+['DX_200' ,'L_250_230','DT_2e-1','BVT',init_date] )

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

### set node count according to 2D vs 3D
if crm_ny<=4: num_nodes = 145
if crm_ny >4: num_nodes = 1013

# Impose wall limits for Summit
# walltime = '2:00'
# walltime = '12:00'
if 'walltime' not in locals():
   if num_nodes>=  1: walltime =  '2:00'
   if num_nodes>= 46: walltime =  '6:00'
   if num_nodes>= 92: walltime = '12:00'
   if num_nodes>=922: walltime = '24:00'


### add these modifiers to enable debug mode or state variable checks
# case += '.debug-on'
# case += '.checks-on'

### specify atmos initial condition file
# init_file_dir = '/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/init_files'
init_file_dir = '/gpfs/alpine/scratch/hannah6/cli115/HICCUP/data'
init_file_sst = f'HICCUP.sst_noaa.{init_date}.c20210625.nc'
params = [p.split('_') for p in case.split('.')]
for p in params:
   if p[0]=='L': init_file_atm = f'HICCUP.atm_era5.{init_date}.ne{ne}np4.L{p[1]}.c20210625.nc'

### specify land initial condition file
land_init_path = '/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/init_files'
land_init_file = 'CLM_spinup.ICRUELM.ne45pg2_r05_oECv3.20-yr.2010-10-01.elm.r.2006-01-01-00000.nc'
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
print('\n  case : '+case+'\n')

dtime = 20*60   # GCM physics time step

if 'dtime' in locals(): ncpl  = 86400 / dtime

num_dyn = ne*ne*6

if 'task_per_node' not in locals():
   if arch=='GNUCPU': task_per_node = 48
   if arch=='GNUGPU': task_per_node = 12
#-------------------------------------------------------------------------------
# Define run command
#-------------------------------------------------------------------------------
# Set up terminal colors
class tcolor:
   ENDC,RED,GREEN,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[35m','\033[36m'
def run_cmd(cmd,suppress_output=False,execute=True):
   if suppress_output : cmd = cmd + ' > /dev/null'
   msg = tcolor.GREEN + cmd + tcolor.ENDC
   print(f'\n{msg}')
   if execute: os.system(cmd)
   return
#---------------------------------------------------------------------------------------------------
# Create new case
#---------------------------------------------------------------------------------------------------
if newcase :

   # Check if directory already exists
   if os.path.isdir(case_dir+case): 
      exit(tcolor.RED+"\nThis case already exists! Are you sure you know what you're doing?\n"+tcolor.ENDC)

   cmd = src_dir+'cime/scripts/create_newcase --case '+case_dir+case
   cmd = cmd + ' --compset '+compset+' --res '+grid
   cmd = cmd + ' --pecount '+str(num_nodes*task_per_node)+'x1 '
   if arch=='GNUCPU': cmd = cmd + ' -compiler gnu    '
   if arch=='GNUGPU': cmd = cmd + ' -compiler gnugpu '
   run_cmd(cmd)

   # Change run directory to be next to bld directory
   os.chdir(case_dir+case+'/')
   memberwork = os.getenv('MEMBERWORK')
   run_cmd(f'./xmlchange -file env_run.xml RUNDIR=\'{memberwork}/{acct}/e3sm_scratch/{case}/run\' ' )
   
   run_cmd(f'./xmlchange -file env_mach_pes.xml -id MAX_TASKS_PER_NODE    -val {task_per_node} ')
   run_cmd(f'./xmlchange -file env_mach_pes.xml -id MAX_MPITASKS_PER_NODE -val {task_per_node} ')

#---------------------------------------------------------------------------------------------------
# Configure
#---------------------------------------------------------------------------------------------------
os.chdir(case_dir+case+'/')
if config : 
   #-------------------------------------------------------
   if arch=='GNUGPU': 
      pcols = np.ceil( (ne**2*6*npg**2) / (num_nodes*task_per_node) )
      run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -pcols {int(pcols)} \" ' )
   #-------------------------------------------------------
   if 'init_file_atm' in locals():
      file = open('user_nl_eam','w')
      file.write(f' ncdata = \'{init_file_dir}/{init_file_atm}\'\n')
      file.close()
   #-------------------------------------------------------
   # Specify CRM columns
   params = [p.split('_') for p in case.split('.')]
   for p in params:
      # if p[0]=='CRMNX': run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -crm_nx {p[1]} \" ')
      # if p[0]=='CRMNY': run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -crm_ny {p[1]} \" ')
      if p[0]=='DT': run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -crm_dt {float(p[1])} \" ')
      if p[0]=='DX': run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -crm_dx {p[1]} \" ')
      # if p[0]=='L' : run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -nlev   {p[1]}  \" ')
      # if p[0]=='NZ': run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -crm_nz {p[1]} \" ')
      if p[0]=='L' : run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -nlev {p[1]} -crm_nz {p[2]}  \" ')
         # nlev = p[1]; crm_nz = None
         # if 'CRMNZ' not in case:
         #    if nlev== '50': crm_nz =  '46'
         #    if nlev=='100': crm_nz =  '95'
         #    if nlev=='120': crm_nz = '115'
         # else:
         #    for m in params:
         #       if m[0]=='CRMNZ': crm_nz = m[1]
         # if crm_nz is None: raise ValueError('No value of crm_nz specified')
         # run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -nlev {p[1]} -crm_nz {crm_nz} \" ')
   #-------------------------------------------------------
   # add in the settings that are specified above
   rad_ny = rad_nx if crm_ny>1 else 1
   # run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -crm_dx {crm_dx} -crm_dt {crm_dt}  \" ')
   run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -crm_nx {crm_nx} -crm_ny {crm_ny} \" ')
   run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -crm_nx_rad {rad_nx} -crm_ny_rad {rad_ny} \" ')
   #-------------------------------------------------------
   # Add special MMF options based on case name
   cpp_opt = ''
   if crm_ny>1 : 
      cpp_opt += ' -DMMF_MOMENTUM_FEEDBACK'
   else:
      cpp_opt += ' -DMMF_DIR_NS '
   # if '.MOMFB.'  in case: cpp_opt += ' -DMMF_MOMENTUM_FEEDBACK'
   if 'debug-on' in case: cpp_opt += ' -DYAKL_DEBUG'

   # convective varance transport
   if any(x in case for x in ['.BVT.','.FVT']): 
      run_cmd(f'./xmlchange --append -id CAM_CONFIG_OPTS -val \" -use_MMF_VT \" ')
   # if '.FVT' in case: 
   #    params = [p.split('_') for p in case.split('.')]
   #    for p in params:
   #       if p[0]=='FVT': cpp_opt += f' -DMMF_VT_KMAX={p[1]} '

   if cpp_opt != '' :
      cmd  = f'./xmlchange --append -file env_build.xml -id CAM_CONFIG_OPTS'
      cmd += f' -val \" -cppdefs \'{cpp_opt} \'  \" '
      run_cmd(cmd)
   #-------------------------------------------------------
   # Set tasks and threads
   if arch == 'GNUGPU':
      atm_ntasks = task_per_node*num_nodes
      cmd = './xmlchange -file env_mach_pes.xml '
      cmd += f' NTASKS_ATM={atm_ntasks}'
      if ne==4:  alt_ntask = task_per_node*4
      if ne==45: alt_ntask = task_per_node*6*4
      cmd += f',NTASKS_LND={alt_ntask},NTASKS_CPL={alt_ntask}'
      cmd += f',NTASKS_OCN={alt_ntask},NTASKS_ICE={alt_ntask}'
      alt_ntask = task_per_node
      cmd += f',NTASKS_ROF={alt_ntask},NTASKS_WAV={alt_ntask},NTASKS_GLC={alt_ntask}'
      cmd += f',NTASKS_ESP=1,NTASKS_IAC=1'
      run_cmd(cmd)
      # run_cmd('./xmlchange -file env_mach_pes.xml NTHRDS_ATM=2,NTHRDS_CPL=2,NTHRDS_LND=1')
   #-------------------------------------------------------
   # 64_data format is needed for large numbers of columns (GCM or CRM)
   run_cmd('./xmlchange PIO_NETCDF_FORMAT=\"64bit_data\" ')
   #-------------------------------------------------------
   # Run case setup
   if clean : run_cmd('./case.setup --clean')
   run_cmd('./case.setup --reset')
#---------------------------------------------------------------------------------------------------
# Build
#---------------------------------------------------------------------------------------------------
if build : 
   # run_cmd('./xmlchange PIO_VERSION=1')
   if 'debug-on' in case : run_cmd('./xmlchange -file env_build.xml -id DEBUG -val TRUE ')
   if clean : run_cmd('./case.build --clean')
   run_cmd('./case.build')
#---------------------------------------------------------------------------------------------------
# Write the namelist options and submit the run
#---------------------------------------------------------------------------------------------------
if submit : 
   # Change inputdata from default due to permissions issue
   run_cmd('./xmlchange DIN_LOC_ROOT=/gpfs/alpine/cli115/scratch/hannah6/inputdata ')
   
   #-------------------------------------------------------
   # First query some stuff about the case
   #-------------------------------------------------------
   (din_loc_root, err) = sp.Popen('./xmlquery DIN_LOC_ROOT    -value', \
                                     stdout=sp.PIPE, shell=True, universal_newlines=True).communicate()
   (config_opts, err) = sp.Popen('./xmlquery CAM_CONFIG_OPTS -value', \
                                     stdout=sp.PIPE, shell=True, universal_newlines=True).communicate()
   config_opts = ' '.join(config_opts.split())   # remove extra spaces to simplify string query
   ntasks_atm = None
   (ntasks_atm     , err) = sp.Popen('./xmlquery NTASKS_ATM    -value', \
                                     stdout=sp.PIPE, shell=True, universal_newlines=True).communicate()
   ntasks_atm = float(ntasks_atm)
   #-------------------------------------------------------
   # Namelist options
   #-------------------------------------------------------
   nfile = 'user_nl_eam'
   file = open(nfile,'w') 
   #------------------------------
   # Specify history output frequency and variables
   #------------------------------   
   file.write(' nhtfrq    = 0,-1 \n') 
   file.write(' mfilt     = 1, 24 \n') # 1-day files
   file.write(" fincl2    = 'PS','PSL','TS'")
   file.write(             ",'PRECT','TMQ'")
   file.write(             ",'LHFLX','SHFLX'")             # surface fluxes
   file.write(             ",'FSNT','FLNT'")               # Net TOM heating rates
   file.write(             ",'FLNS','FSNS'")               # Surface rad for total column heating
   file.write(             ",'FSNTC','FLNTC'")             # clear sky heating rates for CRE
   file.write(             ",'FLUT','FSNTOA'")
   file.write(             ",'LWCF','SWCF'")               # cloud radiative foricng
   file.write(             ",'TGCLDLWP','TGCLDIWP'")       # liq/ice water path
   file.write(             ",'TAUX','TAUY'")               # surface stress
   file.write(             ",'TBOT','QBOT'")
   file.write(             ",'T700','Q700'")
   file.write(             ",'T','Q','Z3'")                # 3D thermodynamic budget components
   file.write(             ",'U','V','OMEGA'")             # 3D velocity components
   file.write(             ",'CLOUD','CLDLIQ','CLDICE'")   # 3D cloud fields
   file.write(             ",'QRL','QRS'")                 # 3D radiative heating 
   file.write(             ",'QRLC','QRSC'")               # 3D clearsky radiative heating 
   file.write(             ",'PTTEND','PTEQ'")             # 3D physics tendencies
   file.write(             ",'PTECLDLIQ','PTECLDICE'")     # 3D physics tendencies
   file.write(             ",'TOT_DU','TOT_DV'")          # total momentum tendencies
   file.write(             ",'DYN_DU','DYN_DV'")          # Dynamics momentum tendencies
   file.write(             ",'GWD_DU','GWD_DV'")          # 3D gravity wave tendencies
   file.write(             ",'DUV','DVV'")                # 3D PBL tendencies
   if 'use_MMF' in config_opts :
      file.write(          ",'MMF_TK','MMF_TKE','MMF_TKES','MMF_TKEW'")
      file.write(          ",'MMF_PFLX','MMF_QTFLX'")
      # file.write(          ",'MMF_TVFLUX'")  # output is all zeroes!
      if 'MMF_MOMENTUM_FEEDBACK' in config_opts  :
         # file.write(       ",'MMF_UFLX','MMF_VFLX'")
         file.write(       ",'MMF_DU','MMF_DV'")
   file.write('\n')
   #------------------------------
   # Other namelist stuff
   #------------------------------
   # file.write(' srf_flux_avg = 1 \n')              # sfc flux smoothing (for MMF stability)
   # file.write(f' crm_accel_factor = 3 \n')         # CRM acceleration factor (default is 2)

   if num_dyn<ntasks_atm: file.write(' dyn_npes = '+str(num_dyn)+' \n')   # limit dynamics tasks

   if 'checks-on' in case: file.write(' state_debug_checks = .true. \n')

   file.write(" inithist = \'NONE\' \n") # ENDOFRUN / NONE

   if 'L_250' in case: 
      file.write(' se_tstep = 120 \n')
      file.write(' dt_remap_factor = 2 \n')
      file.write(' dt_tracer_factor = 10 \n')
      file.write(' hypervis_subcycle_q = 10 \n')

   # if 'init_file_atm' in locals():
   #    file.write(f' ncdata = \'{init_file_dir}/{init_file_atm}\'\n')

   file.close()
   #-------------------------------------------------------
   # ELM namelist
   #-------------------------------------------------------
   if 'land_init_file' in locals():
      nfile = 'user_nl_elm'
      file = open(nfile,'w')
      file.write(f' finidat = \'{land_init_path}/{land_init_file}\' \n')
      file.close()

   #-------------------------------------------------------
   # Special stuff for hindcast mode
   #-------------------------------------------------------
   os.system(f'./xmlchange -file env_run.xml      RUN_STARTDATE={init_date}')
   os.system(f'./xmlchange -file env_run.xml      SSTICE_DATA_FILENAME={init_file_dir}/{init_file_sst}')
   os.system(f'./xmlchange -file env_run.xml      SSTICE_YEAR_ALIGN={iyr}')
   os.system(f'./xmlchange -file env_run.xml      SSTICE_YEAR_START={iyr}')
   os.system(f'./xmlchange -file env_run.xml      SSTICE_YEAR_END={iyr+1}')
   # os.system('./xmlchange -file env_build.xml    CALENDAR=GREGORIAN)

   file = open('user_nl_eam','a')
   file.write(f' ncdata = \'{init_file_dir}/{init_file_atm}\'\n')
   file.close()

   #-------------------------------------------------------
   # Set some run-time stuff
   #-------------------------------------------------------
   if 'ncpl' in locals(): run_cmd(f'./xmlchange ATM_NCPL={str(ncpl)}')
   run_cmd(f'./xmlchange STOP_OPTION={stop_opt},STOP_N={stop_n},RESUBMIT={resub}')
   run_cmd(f'./xmlchange JOB_QUEUE=batch,JOB_WALLCLOCK_TIME={walltime}')
   run_cmd(f'./xmlchange CHARGE_ACCOUNT={acct},PROJECT={acct}')

   # Restart Frequency
   run_cmd(f'./xmlchange -file env_run.xml REST_OPTION={stop_opt},REST_N={stop_n}')
   # run_cmd('./xmlchange -file env_run.xml REST_OPTION=nmonths,REST_N=2')

   if continue_run :
      run_cmd('./xmlchange -file env_run.xml CONTINUE_RUN=TRUE ')   
   else:
      run_cmd('./xmlchange -file env_run.xml CONTINUE_RUN=FALSE ')
   #-------------------------------------------------------
   # Submit the run
   #-------------------------------------------------------
   run_cmd('./case.submit')
#---------------------------------------------------------------------------------------------------
# Print the case name again
#---------------------------------------------------------------------------------------------------
print(f'\n  case : {case}\n')
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
