#!/usr/bin/env python3
#---------------------------------------------------------------------------------------------------
class clr:END,RED,GREEN,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[35m','\033[36m'
def run_cmd(cmd): print('\n'+clr.GREEN+cmd+clr.END); os.system(cmd); return
#---------------------------------------------------------------------------------------------------
import os, subprocess as sp
newcase,config,build,clean,submit,continue_run,reset_resub,st_archive = False,False,False,False,False,False,False,False

acct = 'atm146'
src_dir  = os.getenv('HOME')+'/E3SM/E3SM_SRC5' # whannah/2024-aqua-res-ensemble

# src_dir  = os.getenv('HOME')+'/E3SM/E3SM_SRC3' # master @ 2024-06-27
# src_dir  = os.getenv('HOME')+'/E3SM/E3SM_SRC3' # master @ 2024-06-17
# src_dir  = os.getenv('HOME')+'/E3SM/E3SM_SRC3' # master @ 2024-06-03

# clean        = True
newcase      = True
config       = True
build        = True
submit       = True
# continue_run = True
### reset_resub  = True

# queue = 'regular' # batch / debug

debug_mode = False

spinup_mode = False
alt_test = False; test_num = 7

if spinup_mode:
   stop_opt,stop_n,resub,walltime = 'ndays',1,1,'0:30'
else:
   # stop_opt,stop_n,resub,walltime = 'ndays',1,1,'0:30' 
   # stop_opt,stop_n,resub,walltime = 'ndays',1,0,'4:00'
   stop_opt,stop_n,resub,walltime = 'ndays',30,0,'6:00'

#---------------------------------------------------------------------------------------------------
comp_list = []
arch_list = []
node_list = []
pert_list = []
ne_list = []
def add_case(comp,ne,arch,node,pert):
   comp_list.append(comp)
   arch_list.append(arch)
   node_list.append(node)
   pert_list.append(pert)
   ne_list.append(ne)
   
#---------------------------------------------------------------------------------------------------

alt_init_file = None

# add_case( 'FAQP-MMF1',  30, 'GNUGPU',   16, 0)
# add_case( 'FAQP-MMF1',  45, 'GNUGPU',   36, 0)
# add_case( 'FAQP-MMF1',  60, 'GNUGPU',   64, 0)
# add_case( 'FAQP-MMF1',  90, 'GNUGPU',  144, 0)
# add_case( 'FAQP-MMF1', 120, 'GNUGPU',  256, 0)
# add_case( 'FAQP-MMF1', 180, 'GNUGPU',  576, 0)
# add_case( 'FAQP-MMF1', 240, 'GNUGPU', 1024, 0)

# add_case( 'FAQP-MMF1',  30, 'GNUGPU',   16, 4)
# add_case( 'FAQP-MMF1',  45, 'GNUGPU',   36, 4)
# add_case( 'FAQP-MMF1',  60, 'GNUGPU',   64, 4)
# add_case( 'FAQP-MMF1',  90, 'GNUGPU',  144, 4)
# add_case( 'FAQP-MMF1', 120, 'GNUGPU',  256, 4)
# add_case( 'FAQP-MMF1', 180, 'GNUGPU',  576, 4)
# add_case( 'FAQP-MMF1', 240, 'GNUGPU', 1024, 4)

# add_case( 'FAQP',       30, 'GNUCPU',   16, 0)
# add_case( 'FAQP',       45, 'GNUCPU',   36, 0)
# add_case( 'FAQP',       60, 'GNUCPU',   64, 0)
# add_case( 'FAQP',       90, 'GNUCPU',  144, 0)
# add_case( 'FAQP',      120, 'GNUCPU',  256, 0)
# add_case( 'FAQP',      180, 'GNUCPU',  576, 0)
# add_case( 'FAQP',      240, 'GNUCPU', 1024, 0)

# add_case( 'FAQP',       30, 'GNUCPU',   16, 4)
# add_case( 'FAQP',       45, 'GNUCPU',   36, 4)
# add_case( 'FAQP',       60, 'GNUCPU',   64, 4)
# add_case( 'FAQP',       90, 'GNUCPU',  144, 4)
# add_case( 'FAQP',      120, 'GNUCPU',  256, 4)
# add_case( 'FAQP',      180, 'GNUCPU',  576, 4)
# add_case( 'FAQP',      240, 'GNUCPU', 1024, 4)

add_case( 'FAQP-MMF1',  30, 'GNUGPU',   32, 0)
# add_case( 'FAQP-MMF1',  30, 'GNUGPU',   32, 4)
# add_case( 'FAQP-MMF1', 120, 'GNUGPU',  512, 0)
# add_case( 'FAQP-MMF1', 120, 'GNUGPU',  512, 4)

# scratch = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch'
# add_case( 'FAQP-MMF1', 120, 'GNUGPU',  512, 4); spinup_case = 'E3SM.2024-AQP-CESS-00.FAQP-MMF1.GNUGPU.ne120pg2_ne120pg2.NN_512.SSTP_4K.SPINUP.GDT_15'; adj_init_file = f'{scratch}/{spinup_case}/run/{spinup_case}.eam.i.'

add_case( 'FAQP',       30, 'GNUCPU',   32, 0)
# add_case( 'FAQP',       30, 'GNUCPU',   32, 4)
# add_case( 'FAQP',      120, 'GNUCPU',  512, 0)
# add_case( 'FAQP',      120, 'GNUCPU',  512, 4)


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def main(compset,ne,arch,num_nodes,sst_pert):

   if 'F2010' in compset: grid = f'ne{ne}pg2_oECv3'
   if 'FAQP'  in compset: grid = f'ne{ne}pg2_ne{ne}pg2'
   case_list = ['E3SM','2024-AQP-CESS-00',compset,arch,grid,f'NN_{num_nodes}',f'SSTP_{sst_pert}K']

   case='.'.join(case_list)

   if alt_test: case = case.replace('2024-AQP-CESS-00',f'2024-AQP-CESS-00-TEST{test_num}')

   if debug_mode: case = case+'.DEBUG'

   # print(case); exit()
   #------------------------------------------------------------------------------------------------
   # if ne== 30: gcm_dt = 20.00*60
   # if ne== 45: gcm_dt = 15.00*60
   # if ne== 60: gcm_dt = 10.00*60
   # if ne== 90: gcm_dt =  7.50*60
   # if ne==120: gcm_dt =  5.00*60
   # if ne==180: gcm_dt =  3.75*60
   # if ne==240: gcm_dt =  2.50*60
   if ne== 30: gcm_dt = 16*60
   if ne== 45: gcm_dt = 12*60
   if ne== 60: gcm_dt =  8*60
   if ne== 90: gcm_dt =  6*60
   if ne==120: gcm_dt =  4*60
   if ne==180: gcm_dt =  3*60
   if ne==240: gcm_dt =  2*60

   if spinup_mode:
      crm_dt = 2
      gcm_dt = gcm_dt / 4
      # gcm_dt = gcm_dt / 8
      case += f'.SPINUP'
      case += f'.GDT_{int(gcm_dt)}'
      case += f'.CDT_{int(crm_dt)}'
   
   # print(case)
   # exit()

   #------------------------------------------------------------------------------------------------
   if 'CPU' in arch: max_mpi_per_node,atm_nthrds  =  42,1 ; max_task_per_node = 42
   if 'GPU' in arch: max_mpi_per_node,atm_nthrds  =   6,7 ; max_task_per_node = 6
   atm_ntasks = max_mpi_per_node*num_nodes
   scratch = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch/'
   case_root = f'{scratch}/{case}'
   #------------------------------------------------------------------------------------------------
   print('\n  case : '+case+'\n')
   #------------------------------------------------------------------------------------------------
   # Create new case
   if newcase : 
      if os.path.isdir(case_root): exit(f'\n{clr.RED}This case already exists!{clr.END}\n')
      cmd = f'{src_dir}/cime/scripts/create_newcase --case {case}'
      cmd += f' --output-root {case_root} '
      cmd += f' --script-root {case_root}/case_scripts '
      cmd += f' --handle-preexisting-dirs u '
      cmd += f' --compset {compset} --res {grid} '
      cmd += f' --project {acct} --walltime {walltime} '
      if arch=='GNUCPU' : cmd += f' -mach summit -compiler gnu    -pecount {atm_ntasks}x{atm_nthrds} '
      if arch=='GNUGPU' : cmd += f' -mach summit -compiler gnugpu -pecount {atm_ntasks}x{atm_nthrds} '
      run_cmd(cmd)
   #------------------------------------------------------------------------------------------------
   os.chdir(f'{case_root}/case_scripts')
   #------------------------------------------------------------------------------------------------
   # Configure
   if config :
      run_cmd(f'./xmlchange EXEROOT={case_root}/bld ')
      run_cmd(f'./xmlchange RUNDIR={case_root}/run ')
      #-------------------------------------------------------
      # when specifying ncdata, do it here to avoid an error message
      write_atm_nl_opts(compset,ne,gcm_dt,alt_init_file)
      #-------------------------------------------------------
      # update nlev
      if compset=='FAQP'     : 
         run_cmd(f'./xmlchange --append --id CAM_CONFIG_OPTS --val \" -nlev 80 -rad  rrtmgp \" ')
      if compset=='FAQP-MMF1':
         run_cmd(f'./xmlchange --append --id CAM_CONFIG_OPTS --val \" -nlev 72 -crm_nz 60 \" ')
      #-------------------------------------------------------
      if spinup_mode and compset=='FAQP-MMF1':
         run_cmd(f'./xmlchange --append --id CAM_CONFIG_OPTS --val \" -crm_dt {crm_dt} \" ')
      #-------------------------------------------------------
      # cpp_opt = ''
      # cpp_opt += f' -DAQP_CESS -DAQP_CESS_PERTURBATION={sst_pert}'
      # if cpp_opt != '' :
      #    cmd  = f'./xmlchange --append --file env_build.xml --id CAM_CONFIG_OPTS'
      #    cmd += f' --val \" -cppdefs \' {cpp_opt} \'  \" '
      #    run_cmd(cmd)
      #-------------------------------------------------------
      run_cmd('./xmlchange PIO_NETCDF_FORMAT=\"64bit_data\" ')
      # run_cmd('./xmlchange PIO_VERSION=1 ')
      #-------------------------------------------------------
      if clean : run_cmd('./case.setup --clean')
      run_cmd('./case.setup --reset')
   #------------------------------------------------------------------------------------------------
   # Build
   if build : 
      if debug_mode : run_cmd('./xmlchange --file env_build.xml --id DEBUG --val TRUE ')
      if clean : run_cmd('./case.build --clean')
      run_cmd('./case.build')
   #------------------------------------------------------------------------------------------------
   # Write the namelist options and submit the run
   if submit : 
      write_atm_nl_opts(compset,ne,gcm_dt,alt_init_file)
      #-------------------------------------------------------
      # # Modified SST file
      # if sst_pert==0:
      #    DIN_LOC_ROOT = '/global/cfs/cdirs/e3sm/inputdata'
      #    # DIN_LOC_ROOT = '/gpfs/alpine/cli115/world-shared/e3sm/inputdata'
      #    sst_file_path = f'{DIN_LOC_ROOT}/ocn/docn7/SSTDATA/'
      #    sst_file_name = 'sst_ice_CMIP6_DECK_E3SM_1x1_2010_clim_c20190821.nc'
      # else:
      #    sst_file_path = '/pscratch/sd/w/whannah/e3sm_scratch/init_scratch'
      #    # sst_file_path = '/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/init_files'
      #    sst_file_name = f'sst_ice_CMIP6_DECK_E3SM_1x1_2010_clim_c20190821.SSTPERT_{sst_pert}K.nc'
      # os.system(f'./xmlchange --file env_run.xml SSTICE_DATA_FILENAME={sst_file_path}/{sst_file_name}')
      #-------------------------------------------------------
      if not alt_test:
         nfile = 'user_nl_docn'
         file = open(nfile,'w')
         file.write(f' aqua_sst_global_pert = {sst_pert} \n')
         file.close()
      #-------------------------------------------------------
      if gcm_dt is not None: 
         ncpl = int( 86400 / gcm_dt )
         run_cmd(f'./xmlchange ATM_NCPL={ncpl}')
         run_cmd(f'./xmlchange ROF_NCPL={ncpl}')
      #-------------------------------------------------------
      if 'queue'    in globals(): run_cmd(f'./xmlchange JOB_QUEUE={queue}')
      if 'walltime' in globals(): run_cmd(f'./xmlchange JOB_WALLCLOCK_TIME={walltime}')
      if 'stop_opt' in globals(): run_cmd(f'./xmlchange STOP_OPTION={stop_opt}')
      if 'stop_n'   in globals(): run_cmd(f'./xmlchange STOP_N={stop_n}')
      # if 'resub' in globals() and not continue_run: run_cmd(f'./xmlchange RESUBMIT={resub}')
      # if 'resub' in globals() and reset_resub: run_cmd(f'./xmlchange RESUBMIT={resub}')
      if 'resub' in globals(): run_cmd(f'./xmlchange RESUBMIT={resub}')
      #-------------------------------------------------------
      if     continue_run: run_cmd('./xmlchange --file env_run.xml CONTINUE_RUN=TRUE ')   
      if not continue_run: run_cmd('./xmlchange --file env_run.xml CONTINUE_RUN=FALSE ')
      #-------------------------------------------------------
      run_cmd('./xmlchange --file env_run.xml EPS_AGRID=1e-10' )
      #-------------------------------------------------------
      # run_cmd('./xmlchange SAVE_TIMING_DIR=/gpfs/alpine2/atm146/proj-shared' )
      #-------------------------------------------------------
      run_cmd('./case.submit')
   #------------------------------------------------------------------------------------------------
   # Print the case name again
   print(f'\n  case : {case}\n')
#---------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
def write_atm_nl_opts(compset,ne,gcm_dt,alt_init_file=None):
   nfile = 'user_nl_eam'
   file = open(nfile,'w')
   file.write(' empty_htapes = .true. \n')
   file.write(' nhtfrq = -240,-1,-1 \n')
   file.write(' mfilt  = 1,24,24 \n')
   file.write('\n')
   file.write(" fincl1 = 'CLDHGH','CLDICE','CLDICEZM','CLDLIQ','CLDLIQZM','CLDLOW','CLDMED','CLDTOT'")
   file.write(          ",'CLIQSNUM','CLOUD','CLOUDFRAC_CLUBB','CONCLD','CRAINNUM','CSNOWNUM'")
   file.write(          ",'DCQ','DIFZM','DLFZM','DMS_SRF','DNIFZM','DNLFZM','DTCOND','DTENDTH','DTENDTQ','FICE'")
   file.write(          ",'FLDS','FLNS','FLNSC','FLNT','FLNTC','FLUT','FLUTC'")
   file.write(          ",'FREQI','FREQL','FREQR','FREQS','FRZZM','FSDS','FSDSC','FSNS','FSNSC','FSNT','FSNTC'")
   file.write(          ",'FSNTOA','FSNTOAC','FSUTOA','FSUTOAC'")
   file.write(          ",'H2O2_SRF','H2SO4_SRF','ICEFRAC','ICIMR','ICWMR','IWC'")
   file.write(          ",'LANDFRAC','LHFLX','LWCF','OCNFRAC','OMEGA','OMEGA500','OMEGAT'")
   file.write(          ",'PBLH','PHIS','PRECC','PRECL','PRECSC','PRECSL','PS','PSL'")
   file.write(          ",'Q','QFLX','QGRAPZM','QNGZM','QNIZM','QNLZM','QNRZM','QNSZM','QRAINZM','QREFHT'")
   file.write(          ",'QRL','QRS','QSNOWZM','RAINQM','RELHUM','SCO','SHFLX'")
   # file.write(          ",'SNOWHICE','SNOWHLND','SNOWQM'")
   file.write(          ",'SOLIN','SWCF','T','TAUGWX','TAUGWY','TAUX','TAUY','TCO'")
   file.write(          ",'TGCLDCWP','TGCLDIWP','TGCLDLWP','TH7001000','TMQ'")
   # file.write(          ",'TOT_CLD_VISTAU','TOT_ICLD_VISTAU','TOZLNZ','TREFHT'")
   file.write(          ",'TROP_P','TROP_T','TS','TSMN','TSMX','TUH','TUQ','TVH','TVQ'")
   file.write(          ",'U','U10','UU','V','VQ','VT','VU','VV','WSUB','WUZM','Z3'")
   file.write(          ",'CLOUD','CLDLIQ','CLDICE'")
   file.write('\n')
   file.write(" fincl2 = 'PS','PSL','TS'")
   file.write(          ",'PRECT','TMQ'")
   file.write(          ",'LHFLX','SHFLX'")             # surface fluxes
   file.write(          ",'FLDS','FLNS'")               # sfc LW
   file.write(          ",'FSDS','FSNS'")               # sfc SW
   file.write(          ",'FLDSC','FLNSC'")             # sfc LW clearsky
   file.write(          ",'FSDSC','FSNSC'")             # sfc SW clearsky
   # file.write(          ",'FLUTOA','FLNTOA'")           # toa LW
   # file.write(          ",'FSUTOA','FSNTOA'")           # toa SW
   # file.write(          ",'FLUTOAC','FLNTOAC'")         # toa LW clearsky
   # file.write(          ",'FSUTOAC','FSNTOAC'")         # toa SW clearsky
   file.write(          ",'FSNT','FLNT'")               # Net TOM rad
   file.write(          ",'FLNS','FSNS'")               # Net Sfc rad
   file.write(          ",'FSNTC','FLNTC'")             # clear sky heating rates for CRE
   file.write(          ",'TGCLDLWP','TGCLDIWP'")       # liq & ice water path
   file.write(          ",'CLDTOT','CLDLOW','CLDMED','CLDHGH'")
   file.write(          ",'TUQ','TVQ'")                 # vapor transport for AR tracking
   # variables for tracking stuff like hurricanes
   file.write(          ",'TBOT:I','QBOT:I','UBOT:I','VBOT:I'") # lowest model level
   file.write(          ",'T900:I','Q900:I','U900:I','V900:I'") # 900mb data
   file.write(          ",'T850:I','Q850:I','U850:I','V850:I'") # 850mb data
   file.write(          ",'Z300:I','Z500:I'")
   file.write(          ",'OMEGA850:I','OMEGA500:I'")
   file.write(          ",'U200:I','V200:I'")
   file.write('\n')
   if alt_init_file is None:
      # init_root = '/pscratch/sd/w/whannah/HICCUP'
      init_root = '/gpfs/alpine2/atm146/proj-shared/hannah6/HICCUP'
      init_file = None
      if compset=='FAQP':
         init_file = f'{init_root}/eam_i_aquaplanet_ne{ne}np4_L80-eam_c20240607.nc'
      if compset=='FAQP-MMF1':
         init_file = f'{init_root}/eam_i_aquaplanet_ne{ne}np4_L72-mmf_c20240607.nc'
      if init_file is not None:
         file.write(f" ncdata  = '{init_file}' \n")
   else:
      file.write(f" ncdata  = '{alt_init_file}' \n")
   if 'FAQP' in compset:
      # disable all aerosol rad for all cases
      file.write(f" do_aerosol_rad  = .false. \n")
   factor = 5
   se_tstep = gcm_dt/factor
   file.write(f'se_tstep = {se_tstep} \n')
   file.write(f'hypervis_subcycle_q = {factor} \n')
   file.write(f'dt_tracer_factor = {factor} \n')
   file.write(f'dt_remap_factor = 1 \n')
   if spinup_mode: file.write('inithist = \'ENDOFRUN\' \n')
   file.close()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
if __name__ == '__main__':

   for n in range(len(comp_list)):
      # print('-'*80)
      main( comp_list[n], \
            ne_list[n], \
            arch_list[n], \
            node_list[n], \
            pert_list[n], \
           )
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
